import React from 'react';
import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import SendMoney from '../screens/SentMoney';

const RootStack = createStackNavigator({
  Main : {screen : MainTabNavigator},
},{
  headerMode : 'none'
},{
  initialRouteName : 'Main'
})

export default createAppContainer(createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  RootStack
}));