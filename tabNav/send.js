import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  Image,
  TouchableHighlight,
  Alert,
  SectionList,
  ImageBackground
} from "react-native";
import { Container, Header, Content, Tab, Tabs, Item, Icon, Input, List, ListItem, Separator, Right } from 'native-base'

const { width: deviceWidth, height: deviceHeight } = Dimensions.get("window");

const SLIDES = [
  {
    VisaLogo: "VISA",
    visaNumber : "8014",
    cardName : "Fransiskus Arnoldy",
    cardSaldo : "$ 1.998,28",
    backgroundColor: "#8850FF",
    touchableBackgroundColor: "#CAB1FF",
  },
  {
    VisaLogo: "MASTER",
    visaNumber : "8014",
    cardName : "Fransiskus Arnoldy",
    cardSaldo : "$ 1.998,28",
    backgroundColor: "#FFBA00",
    touchableBackgroundColor: "#FFDB79",
  },
  {
    VisaLogo: "DEBIT",
    visaNumber : "8014",
    cardName : "Fransiskus Arnoldy",
    cardSaldo : "$ 1.998,28",
    backgroundColor: "#8676FB",
    touchableBackgroundColor: "#9EA8FF",
  }
];
const sentTransaction = [
  {title: 'Today', data: ['Catharina','Siena','Fransiena','FranshiroMedia']},
  {title: 'Yesterday', data: ['Jackson', 'James', 'Jillian', 'Jimmy', 'Joel', 'John', 'Julie','frans','agus','ini data baru']},
]

class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection:"row", justifyContent:"space-between", marginHorizontal:16, top:40, height:42}}>
          <Text style={styles.balance}>Send Money</Text>
          <Image style={styles.profile} source={require('../img/shoe1.png')} />
        </View>
        <Container style={{top:70}}>
                  <Tabs locked={true} tabBarUnderlineStyle={{borderBottomWidth:2, borderBottomColor:"#f20a0a"}}>
                    <Tab heading="Sent"
                      tabStyle={{backgroundColor:"#e4e8ed"}}
                      textStyle={{
                        fontStyle:"normal",
                        fontWeight:"600",
                        fontSize:18,
                        letterSpacing:0.1,
                        color:"#A6AAB4"
                      }}
                      activeTabStyle={{
                        backgroundColor:"#e4e8ed",
                      }}
                      activeTextStyle={{
                        fontStyle:"normal",
                        color:"#3B414B",
                        fontWeight:"600",
                        fontSize:18,
                      }}
                    >
                      <Content style={{backgroundColor:"#e4e8ed"}}>
                        <Text style={{
                            top : 40,
                            left: 20,
                            fontStyle:"normal",
                            fontWeight:"500",
                            fontSize:12,
                            lineHeight:18,
                            letterSpacing:0.2,
                            color:"#A6AAB4"
                        }}>
                          Select Credit Card  
                        </Text>
                        <View style={{flexDirection:"row", justifyContent:"center"}}>
                            <View style={styles.miniCardRightContainer}>
                                <ScrollView
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    scrollEventThrottle={16}
                                >
                                {SLIDES.map((slide, index) => {
                                    return (
                                    <View key={index} style={styles.cardContainer}>
                                        <View style={[styles.card,{backgroundColor:slide.backgroundColor}]}>
                                        <View style={styles.cardTopContainer}>
                                            <View>
                                            <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                                                <Text style={styles.VisaLogoText}>
                                                {slide.VisaLogo.toUpperCase()}
                                                </Text>
                                            </View>
                                            </View>
                                            <View style={styles.visaNumber}>
                                            <Text style={styles.visaNumberBlock}>* * * *</Text>
                                            <Text style={styles.visaNumberBlock}>{slide.visaNumber}</Text>
                                            </View>
                                            <View style={{flexDirection:"row", justifyContent:"space-between",top:46}}>
                                            <Text style={styles.cardHolderExpiresText}>{slide.cardSaldo}</Text>
                                            </View>
                                        </View>
                                        </View>
                                    </View>
                                    );
                                })}
                                </ScrollView>
                            </View>
                            <View style={[styles.miniCardLeftContainer]}>
                              <View style={[styles.cardContainer]}>
                                <View style={[styles.newCard, {alignItems:"center"}]}>
                                  <View>
                                    <View style={{
                                      flex:1,
                                      flexDirection:"column",
                                      alignItems:"center",
                                    }}>
                                      <View style={{
                                        width:40,
                                        height:40,
                                        marginHorizontal:40,
                                        top: 16,
                                        borderRadius:20,
                                        backgroundColor:"#10C971",
                                        justifyContent:"center",
                                        alignItems:"center"
                                      }}>
                                        <Text style={{
                                          fontSize:24,
                                          color:"white"
                                        }}> + </Text>
                                      </View>
                                      <Text style={{top: 25}}>New</Text>
                                      <Text style={{top: 30}}> Credit Card</Text>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            </View>
                        </View>
                        <Text style={{
                            top : 40,
                            left: 20,
                            fontStyle:"normal",
                            fontWeight:"500",
                            fontSize:12,
                            lineHeight:18,
                            letterSpacing:0.2,
                            color:"#A6AAB4"
                        }}>
                          Select Credit Card  
                        </Text>
                        <View>
                            <View style={styles.recipientContainer}>
                                <ScrollView
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    scrollEventThrottle={16}
                                >
                                {SLIDES.map((slide, index) => {
                                    return (
                                    <View key={index} style={styles.cardContainer}>
                                        <View style={[styles.card,{backgroundColor:slide.backgroundColor}]}>
                                        <View style={styles.cardTopContainer}>
                                            <View>
                                            <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                                                <Text style={styles.VisaLogoText}>
                                                {slide.VisaLogo.toUpperCase()}
                                                </Text>
                                            </View>
                                            </View>
                                            <View style={styles.visaNumber}>
                                            <Text style={styles.visaNumberBlock}>* * * *</Text>
                                            <Text style={styles.visaNumberBlock}>{slide.visaNumber}</Text>
                                            </View>
                                            <View style={{flexDirection:"row", justifyContent:"space-between",top:46}}>
                                            <Text style={styles.cardHolderExpiresText}>{slide.cardSaldo}</Text>
                                            </View>
                                        </View>
                                        </View>
                                    </View>
                                    );
                                })}
                                </ScrollView>
                            </View>
                        </View>
                        
                      </Content>
                        
                    </Tab>
                    <Tab heading="Receive"
                      tabStyle={{backgroundColor:"#e4e8ed"}}
                      textStyle={{
                        fontStyle:"normal",
                        fontWeight:"600",
                        fontSize:18,
                        letterSpacing:0.1,
                        color:"#A6AAB4"
                      }}
                      activeTabStyle={{
                        backgroundColor:"#e4e8ed"
                      }}
                      activeTextStyle={{
                        fontStyle:"normal",
                        color:"#3B414B",
                        fontWeight:"600",
                        fontSize:18,
                      }}
                    >
                      <Content style={{backgroundColor:"#e4e8ed"}}>

                      </Content>
                    </Tab>
                  </Tabs>
                </Container>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : '#e4e8ed'
  },
  balance:{
    width:274,
    height:52,
    fontStyle:"normal",
    fontWeight:"600",
    fontSize:32,
    lineHeight:42,
    letterSpacing:0.5,
    color: "#171D33"
  },
  profile:{
    width:32,
    height:32,
    borderRadius:16,
    backgroundColor:'#e4e8ed'
  },
  miniCardRightContainer: {
    width: (deviceWidth/2) + 40,
    height: 250,
    paddingLeft:20
  },
  miniCardLeftContainer: {
    height: 250,
    paddingRight: 20
  },
  cardContainer: {
    padding:0,
    margin:0,
    width: deviceWidth/2,
    height: deviceHeight,
    alignItems:"flex-end"
  },
  recipientContainer: {
    paddingRight: 10,
    width: deviceWidth,
    height: deviceHeight,
  },
  card: {
    top: 60,
    width: (deviceWidth/2) - 20,
    height: 160,
    borderRadius: 6,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: {
      width: 0,
      height: 15
    },
    shadowRadius: 25
  },
  newCard:{
    top: 60,
    width: (deviceWidth/2)-25,
    height: 160,
    borderRadius: 6,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: {
      width: 0,
      height: 15
    },
    shadowRadius: 25,
    marginRight:20,
    backgroundColor:"white"
  },
  cardTopContainer: {
    flex: 1,
    overflow: "hidden",
    padding: 10,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: "transparent"
  },
  VisaLogoText: {
    fontSize: 20,
    fontWeight: "800",
    letterSpacing: 1.5,
    color: "white"
  },
  visaNumber:{
    flexDirection:"row",
    justifyContent:"space-between",
    top:25,
  },
  visaNumberBlock:{
    fontStyle:"normal",
    fontWeight:"500",
    lineHeight:26,
    fontSize:18,
    letterSpacing:2,
    color:"white"
  },
  cardHolderExpiresText:{
    fontStyle:"normal",
    fontWeight:"300",
    fontSize:12,
    lineHeight:20,
    letterSpacing:0.4,
    color:"#FFFFFF",
    opacity:0.8
  },
  cardName:{
    fontStyle:"normal",
    fontWeight:"500",
    fontSize:16,
    lineHeight:24,
    letterSpacing:0.1,
    color:"#FFFFFF",
  },
  sectionHeader:{
    marginHorizontal:16,
    marginVertical:6,
    fontStyle:"normal",
    fontWeight:"500",
    fontSize:12,
    lineHeight:18,
    letterSpacing:0.2,
    color:"#757F8C"

  },
  sectionList:{

  },
  buttonContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    marginBottom: 12,
    paddingVertical: 15,
    paddingHorizontal: 64,
    borderWidth: 1.5,
    borderRadius: 40
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "700",
    letterSpacing: 1.5
  },
});

export default Home