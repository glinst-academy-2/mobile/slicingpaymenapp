import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  Image,
  TouchableHighlight,
  Alert,
  SectionList,
  ImageBackground
} from "react-native";
import { Container, Header, Content, Tab, Tabs, Item, Icon, Input, List, ListItem, Separator, Right } from 'native-base'

const { width: deviceWidth, height: deviceHeight } = Dimensions.get("window");

const SLIDES = [
  {
    VisaLogo: "VISA",
    visaNumber : "8014",
    cardName : "Fransiskus Arnoldy",
    cardExpires : "08/21",
    backgroundColor: "#8850FF",
    touchableBackgroundColor: "#CAB1FF",
  },
  {
    VisaLogo: "MASTER CARD",
    visaNumber : "8014",
    cardName : "Fransiskus Arnoldy",
    cardExpires : "08/21",
    backgroundColor: "#FFBA00",
    touchableBackgroundColor: "#FFDB79",
  },
  {
    VisaLogo: "DEBIT",
    visaNumber : "8014",
    cardName : "Fransiskus Arnoldy",
    cardExpires : "08/21",
    backgroundColor: "#8676FB",
    touchableBackgroundColor: "#9EA8FF",
  }
];
const sentTransaction = [
  {title: 'Today', data: ['Catharina','Siena','Fransiena','FranshiroMedia']},
  {title: 'Yesterday', data: ['Jackson', 'James', 'Jillian', 'Jimmy', 'Joel', 'John', 'Julie','frans','agus','ini data baru']},
]

class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.yourBalance}>Your Balance</Text>
        <View style={{flexDirection:"row", justifyContent:"space-between", marginHorizontal:16, top:40, height:42}}>
          <Text style={styles.balance}>$ 926.21</Text>
          <Image style={styles.profile} source={require('../img/shoe1.png')} />
        </View>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          scrollEventThrottle={16}
        >
          {SLIDES.map((slide, index) => {
            return (
              <View key={index} style={styles.cardContainer}>
                <View style={[styles.card,{backgroundColor:slide.backgroundColor}]}>
                  <View style={styles.cardTopContainer}>
                    <View>
                      <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                        <Text style={styles.VisaLogoText}>
                          {slide.VisaLogo.toUpperCase()}
                        </Text>
                        <Text style={styles.VisaLogoText}>...</Text>
                      </View>
                    </View>
                    <View style={styles.visaNumber}>
                      <Text style={styles.visaNumberBlock}>* * * *</Text>
                      <Text style={styles.visaNumberBlock}>* * * *</Text>
                      <Text style={styles.visaNumberBlock}>* * * *</Text>
                      <Text style={styles.visaNumberBlock}>{slide.visaNumber}</Text>
                    </View>
                    <View style={{flexDirection:"row", justifyContent:"space-between",top:46}}>
                      <Text style={styles.cardHolderExpiresText}>CARD HOLDER</Text>
                      <Text style={styles.cardHolderExpiresText}>EXPIRES</Text>
                    </View>
                    <View style={{flexDirection:"row", justifyContent:"space-between",top:46}}>
                      <Text style={styles.cardName}>{slide.cardName}</Text>
                      <Text style={styles.cardName}>{slide.cardExpires}</Text>
                    </View>
                  </View>
                </View>
                <Container style={{top:80}}>
                  <Tabs locked={true} tabBarUnderlineStyle={{borderBottomWidth:2, borderBottomColor:"#f20a0a"}}>
                    <Tab heading="Sent"
                      tabStyle={{backgroundColor:"#e4e8ed"}}
                      textStyle={{
                        fontStyle:"normal",
                        fontWeight:"600",
                        fontSize:18,
                        letterSpacing:0.1,
                        color:"#A6AAB4"
                      }}
                      activeTabStyle={{
                        backgroundColor:"#e4e8ed",
                      }}
                      activeTextStyle={{
                        fontStyle:"normal",
                        color:"#3B414B",
                        fontWeight:"600",
                        fontSize:18,
                      }}
                    >
                      <Content style={{backgroundColor:"#e4e8ed"}}>
                        <Item regular style={{
                          backgroundColor:"#F0F1F4",
                          marginLeft:16,
                          marginRight:16,
                          marginBottom:16,
                          borderRadius:5,
                          height:32,
                          top: 10
                        }}>
                          <Input placeholder="Search Transaction" style={{
                            fontStyle:"normal",
                            fontWeight:"500",
                            fontSize:12,
                            lineHeight:18,
                            letterSpacing:0.2,
                            color:"#A6AAB4"
                          }} />
                          <Icon name="ios-search" />
                        </Item>
                        <View style={{paddingBottom:190}}>
                          <SectionList
                            sections={sentTransaction}
                            renderItem={({item}) => 
                              <View style={{
                                marginHorizontal:16,
                                marginVertical:2,
                                height:60,
                                backgroundColor:"white",
                                borderRadius:6,
                                flexDirection:"row",
                                justifyContent:"space-between"
                              }}>
                                <View style={{flexDirection:"row"}}>
                                  <Image style={[styles.profile,{margin:14, justifyContent:"center"}]} source={require('../img/shoe1.png')} />
                                  <View>
                                    <View style={{flexDirection:"row", marginTop:10}}>
                                      <Text>to : </Text>
                                      <Text style={{color:"black"}}>{item}</Text>
                                    </View>
                                    <Text style={{fontSize:10, marginTop:5, color:'grey'}}>20 January, 2019</Text>
                                  </View>
                                </View>
                                <View style={{
                                  width:100,
                                  height:22,
                                  marginVertical:19,
                                  marginRight:16,
                                  flexDirection:"row",
                                  justifyContent:"flex-end"
                                }}>
                                  <Text style={{
                                    fontStyle:"normal",
                                    fontWeight:"500",
                                    fontSize:16,
                                    lineHeight:24,
                                    letterSpacing:0.1,
                                    color:"#FA2E69",
                                  }}>
                                    $927.21
                                  </Text>
                                </View>
                              </View>
                            }
                            renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                            keyExtractor={(item, index) => index}
                          />
                        </View>
                      </Content>
                        
                    </Tab>
                    <Tab heading="Receive"
                      tabStyle={{backgroundColor:"#e4e8ed"}}
                      textStyle={{
                        fontStyle:"normal",
                        fontWeight:"600",
                        fontSize:18,
                        letterSpacing:0.1,
                        color:"#A6AAB4"
                      }}
                      activeTabStyle={{
                        backgroundColor:"#e4e8ed"
                      }}
                      activeTextStyle={{
                        fontStyle:"normal",
                        color:"#3B414B",
                        fontWeight:"600",
                        fontSize:18,
                      }}
                    >
                      <Content style={{backgroundColor:"#e4e8ed"}}>

                      </Content>
                    </Tab>
                  </Tabs>
                </Container>
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : '#e4e8ed'
  },
  yourBalance:{
    position:"absolute",
    width:200,
    height:20,
    left:16,
    top:26,
    fontStyle:"normal",
    fontWeight:"500",
    fontSize:14,
    lineHeight:20,
    letterSpacing:0.2,
    color:"#A6AAB4"
  },
  balance:{
    width:200,
    height:52,
    fontStyle:"normal",
    fontWeight:"600",
    fontSize:32,
    lineHeight:42,
    letterSpacing:0.5,
    color: "#171D33"
  },
  profile:{
    width:32,
    height:32,
    borderRadius:16,
    backgroundColor:'#e4e8ed'
  },
  cardContainer: {
    width: deviceWidth,
    height: deviceHeight,
    alignItems: "center",
  },
  card: {
    top: 60,
    width: deviceWidth -64,
    height: 194,
    borderRadius: 6,
    shadowColor: "black",
    shadowOpacity: 0.3,
    shadowOffset: {
      width: 0,
      height: 15
    },
    shadowRadius: 25
  },
  cardTopContainer: {
    flex: 3,
    overflow: "hidden",
    padding: 20,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: "transparent"
  },
  VisaLogoText: {
    fontSize: 20,
    fontWeight: "800",
    letterSpacing: 1.5,
    color: "white"
  },
  visaNumber:{
    flexDirection:"row",
    justifyContent:"space-between",
    top:25,
  },
  visaNumberBlock:{
    fontStyle:"normal",
    fontWeight:"500",
    lineHeight:26,
    fontSize:18,
    letterSpacing:2,
    color:"white"
  },
  cardHolderExpiresText:{
    fontStyle:"normal",
    fontWeight:"300",
    fontSize:12,
    lineHeight:20,
    letterSpacing:0.4,
    color:"#FFFFFF",
    opacity:0.8
  },
  cardName:{
    fontStyle:"normal",
    fontWeight:"500",
    fontSize:16,
    lineHeight:24,
    letterSpacing:0.1,
    color:"#FFFFFF",
  },
  sectionHeader:{
    marginHorizontal:16,
    marginVertical:6,
    fontStyle:"normal",
    fontWeight:"500",
    fontSize:12,
    lineHeight:18,
    letterSpacing:0.2,
    color:"#757F8C"

  },
  sectionList:{

  },
  buttonContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    marginBottom: 12,
    paddingVertical: 15,
    paddingHorizontal: 64,
    borderWidth: 1.5,
    borderRadius: 40
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "700",
    letterSpacing: 1.5
  },
});

export default Home