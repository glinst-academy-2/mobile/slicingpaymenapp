import React from 'react';
import { StyleSheet, Image, Text, View } from  'react-native'
import { WebBrowser } from 'expo'
import { Ionicons } from '@expo/vector-icons'
import Touchable from 'react-native-platform-touchable'

import SendScreen from '../tabNav/send'

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    header : null,
  };
  render() {
    return (
      <SendScreen />
    );
  }
}


const styles = StyleSheet.create({
container: {
  flex: 1,
  paddingTop: 15,
},
optionsTitleText: {
  fontSize: 16,
  marginLeft: 15,
  marginTop: 9,
  marginBottom: 12,
},
optionIconContainer: {
  marginRight: 9,
},
option: {
  backgroundColor: '#fdfdfd',
  paddingHorizontal: 15,
  paddingVertical: 15,
  borderBottomWidth: StyleSheet.hairlineWidth,
  borderBottomColor: '#EDEDED',
},
optionText: {
  fontSize: 15,
  marginTop: 1,
},
});
