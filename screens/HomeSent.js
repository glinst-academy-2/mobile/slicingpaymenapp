import React, {Component} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import TabHome from '../tabNav/home'

export default class HomeScreen extends Component {
  static navigationOptions = {
    header : null,
  };

  render() {
    return (
      <TabHome />
    );
  }

}